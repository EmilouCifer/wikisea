//
//  Country.swift
//  Wiki SEA
//
//  Created by Jogja 247 on 18/07/19.
//  Copyright © 2019 Jogja 247. All rights reserved.
//

import UIKit

class Country {
    var title: String
    var subTitle: String
    var nickName: String
    var photo: String
    var timezones: [String]
    var population: Int
    var area: Double
    
    //MARK: Initialization
    init(_ dictionary: [String: Any]) {
        
        // Initialize stored properties.
        self.title = dictionary["name"] as? String ?? ""
        self.subTitle = dictionary["capital"] as? String ?? ""
        self.nickName = dictionary["cioc"] as? String ?? ""
        self.photo = dictionary["flag"] as? String ?? ""
        self.timezones = dictionary["timezones"] as? Array ?? [""]
        self.population = dictionary["population"] as? Int ?? 0
        self.area = dictionary["area"] as? Double ?? 0.0
    }
}
