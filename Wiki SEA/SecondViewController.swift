//
//  SecondViewController.swift
//  Wiki SEA
//
//  Created by Jogja 247 on 18/07/19.
//  Copyright © 2019 Jogja 247. All rights reserved.
//

import UIKit
import Charts
import Alamofire

class SecondViewController: UIViewController {

    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var barChart: BarChartView!
    var country = [Country]()
    let urlCountry = "https://restcountries.eu/rest/v2/regionalbloc/asean"
    
    var dataBar: [BarChartDataEntry] = []
    var dataPie: [PieChartDataEntry] = []
    var dataLabel:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadDataCountry()
        // Do any additional setup after loading the view.
    }

    private func loadDataCountry(){
        Alamofire.request(urlCountry).responseJSON { response in                  // response serialization result
            if let json = response.result.value {
                guard let jsonArray = json as? [[String: Any]] else {
                    return
                }
                
                for objct in jsonArray {
                    self.country.append(Country.init(objct))
                }
                
                for i in 0..<self.country.count {
                    let dataB = BarChartDataEntry(x: Double(i), y: Double(self.country[i].population))
                    let dataP = PieChartDataEntry(value: Double(self.country[i].population))
                    
                    self.dataBar.append(dataB)
                    self.dataPie.append(dataP)
                    self.dataLabel.append(self.country[i].nickName)
                }
                
                self.createBar()
                self.createPie()
            }
        }
    }
    
    private func createBar(){
        barChart.noDataText = "Data not available"
        barChart.drawBarShadowEnabled = false
        barChart.chartDescription?.enabled = true
        barChart.pinchZoomEnabled = false
        barChart.scaleXEnabled = false
        barChart.scaleYEnabled = false
        barChart.leftAxis.drawZeroLineEnabled = false
        barChart.leftAxis.enabled = false
        barChart.rightAxis.drawZeroLineEnabled = false
        barChart.rightAxis.enabled = false
        barChart.legend.enabled = false
        
        let xAxis = barChart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawGridLinesEnabled = false
        xAxis.labelCount = country.count
        xAxis.valueFormatter = IndexAxisValueFormatter(values: dataLabel)
        
        let set = BarChartDataSet(entries: dataBar, label: "Population")
        set.colors = ChartColorTemplates.joyful()
        let barData = BarChartData(dataSet: set)
        barChart.fitBars = true
        barChart.data = barData
    }
    
    private func createPie(){
        pieChart.noDataText = "Data not available"
        pieChart.drawEntryLabelsEnabled = false
        pieChart.legend.enabled = false
        pieChart.rotationEnabled = false
        pieChart.chartDescription?.enabled = false
        pieChart.drawHoleEnabled = true
        pieChart.holeRadiusPercent = 0.35
        pieChart.usePercentValuesEnabled = true
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.percentSymbol = "%"
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0
        
        let set = PieChartDataSet(entries: dataPie, label: "Population")
        set.colors = ChartColorTemplates.joyful()
        set.sliceSpace = 3
        set.valueColors = [UIColor.black]
        let pieData = PieChartData(dataSet: set)
        pieData.setValueFormatter(DefaultValueFormatter(formatter: formatter))
        pieChart.data = pieData
    }
}

