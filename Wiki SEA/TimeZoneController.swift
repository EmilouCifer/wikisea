//
//  TimeZoneController.swift
//  Wiki SEA
//
//  Created by Jogja 247 on 24/07/19.
//  Copyright © 2019 Jogja 247. All rights reserved.
//

import UIKit
import Alamofire
import Charts

class TimeZoneController: UIViewController {
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var pieChart: PieChartView!
    let urlCountry = "https://restcountries.eu/rest/v2/regionalbloc/asean"
    var country = [Country]()
    var dataLabel = [[String]]()
    var allTimeZone = [String]()
    var convert = [[Int]]()
    
    var dataPie: [PieChartDataEntry] = []
    
    var estimateWidth = 160.0
    var cellMarginSize = 16.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDataCountry()
        self.collection.delegate = self
        self.collection.dataSource = self
        self.collection.register(UINib(nibName: "CountryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CountryCollectionViewCell")
        
        self.setupGrid()
    }
    
    func setupGrid() {
        let flow = collection?.collectionViewLayout as! UICollectionViewFlowLayout
        flow.minimumInteritemSpacing = CGFloat(self.cellMarginSize)
        flow.minimumLineSpacing = CGFloat(self.cellMarginSize)
    }
    
    private func loadDataCountry(){
        Alamofire.request(urlCountry).responseJSON { response in                  // response serialization result
            if let json = response.result.value {
                guard let jsonArray = json as? [[String: Any]] else {
                    return
                }
                
                for objct in jsonArray {
                    self.country.append(Country.init(objct))
                    
                }
                
                for i in 0..<self.country.count {
                    let dataP = PieChartDataEntry(value: Double(self.country[i].area))
                    
                    self.dataPie.append(dataP)
                    self.dataLabel.append(self.country[i].timezones)
                }
                
                self.createPie()
                
                let lapan = self.dataLabel
                
                self.allTimeZone = Array(Set(self.dataLabel.joined())).sorted()
                print("\(self.allTimeZone)")
                var greas = [[Int]]()
                var pos: Int = 0
                var str = [String]()
                for x in 0..<lapan.count{
                    if lapan[x].count > 1{
                        pos = x
                        for y in 0..<lapan[x].count {
                            str.append(lapan[x][y])
                        }
                    } else {
                        greas.append(lapan.indices.filter({ lapan[$0] == lapan[x] }))
                    }
                }
                
                self.convert = Array(Set(greas))
                
                for a in 0..<self.convert.count {
                    if self.convert[a][0] == 0 {
                        self.convert[a].append(pos)
                    } else if self.convert[a][0] == 1 {
                        self.convert[a].append(pos)
                    } else {
                        self.convert.append([pos])
                    }
                }
                
                for b in 0..<self.convert.count {
                    for c in 0..<self.convert[b].count {
                        if self.convert[b][c] == 1 {
                            
                        }
                    }
                }
                self.collection.reloadData()
            }
        }
    }
    
    private func createPie(){
        pieChart.noDataText = "Data not available"
        pieChart.drawEntryLabelsEnabled = false
        pieChart.legend.enabled = false
        pieChart.rotationEnabled = false
        pieChart.chartDescription?.enabled = false
        pieChart.drawHoleEnabled = true
        pieChart.holeRadiusPercent = 0.35
        
        let set = PieChartDataSet(entries: dataPie, label: "Population")
        set.colors = ChartColorTemplates.joyful()
        set.sliceSpace = 3
        set.valueColors = [UIColor.black]
        let pieData = PieChartData(dataSet: set)
        pieChart.data = pieData
    }
}
extension TimeZoneController: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allTimeZone.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CountryCollectionViewCell", for: indexPath) as! CountryCollectionViewCell
        let countrys = allTimeZone[indexPath.row]
        if allTimeZone[indexPath.row] == "UTC+06:30" {
            
        }
        /*for z in 0..<self.convert.count {
            if convert[z].count > 1 {
                
            } else {
                
            }
        }*/
        cell.setTimezone(textZone: countrys, textCountry: "")
        //cell.labelZone.text = allTimeZone[indexPath.row]
        
        return cell
        
    }
}

extension TimeZoneController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.calculateWidth()
        return CGSize(width: width, height: width)
    }
    
    func calculateWidth() -> CGFloat {
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
        
        let margin = CGFloat(cellMarginSize * 2)
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        
        return width
    }
}
