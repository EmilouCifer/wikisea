//
//  CountryTableViewCell.swift
//  Wiki SEA
//
//  Created by Jogja 247 on 18/07/19.
//  Copyright © 2019 Jogja 247. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var nickLabel: UILabel!
    @IBOutlet weak var imageFlag: UIWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
