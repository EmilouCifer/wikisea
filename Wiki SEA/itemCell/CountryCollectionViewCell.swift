//
//  CountryCollectionViewCell.swift
//  Wiki SEA
//
//  Created by Jogja 247 on 24/07/19.
//  Copyright © 2019 Jogja 247. All rights reserved.
//

import UIKit

class CountryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var labelZone: UILabel!
    @IBOutlet weak var labelCountry: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setTimezone(textZone: String, textCountry: String) {
        labelZone.text = textZone
        labelCountry.text = textCountry
    }
}
